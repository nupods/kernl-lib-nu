<?php

namespace Kernl\Lib;

class NU
{
    /**
     * Retrieve various Northeastern head/meta tags
     * @return string
     */
    public static function headMeta()
    {
        return '
            <link rel="apple-touch-icon" sizes="57x57"  href="https://brand.northeastern.edu/global/assets/favicon/apple-touch-57x57.png?v=2" /><link rel="apple-touch-icon" sizes="60x60"  href="https://brand.northeastern.edu/global/assets/favicon/apple-touch-60x60.png?v=2" />
            <link rel="apple-touch-icon" sizes="72x72"  href="https://brand.northeastern.edu/global/assets/favicon/apple-touch-72x72.png?v=2" />
            <link rel="apple-touch-icon" sizes="76x76"  href="https://brand.northeastern.edu/global/assets/favicon/apple-touch-76x76.png?v=2" />
            <link rel="apple-touch-icon" sizes="114x114"  href="https://brand.northeastern.edu/global/assets/favicon/apple-touch-114x114.png?v=2" />
            <link rel="apple-touch-icon" sizes="120x120"  href="https://brand.northeastern.edu/global/assets/favicon/apple-touch-120x120.png?v=2" />
            <link rel="apple-touch-icon" sizes="144x144"  href="https://brand.northeastern.edu/global/assets/favicon/apple-touch-144x144.png?v=2" />
            <link rel="apple-touch-icon" sizes="152x152"  href="https://brand.northeastern.edu/global/assets/favicon/apple-touch-152x152.png?v=2" />
            <link rel="icon" sizes="144x144" type="image/png" href="https://brand.northeastern.edu/global/assets/favicon/android-chrome-144x144.png?v=2" />
            <link rel="icon" sizes="32x32" type="image/png" href="https://brand.northeastern.edu/global/assets/favicon/favicon-32x32.png?v=2" />
            <link rel="icon" sizes="16x16" type="image/png" href="https://brand.northeastern.edu/global/assets/favicon/favicon-16x16.png?v=2" />
            <link rel="manifest" href="https://brand.northeastern.edu/global/assets/favicon/manifest.json" />
            <meta name="msapplication-TileColor" content="#ffffff" />
            <meta name="msapplication-TileImage" content="https://brand.northeastern.edu/global/assets/favicon/mstile-144x144.png?v=2" />
            <meta name="theme-color" content="#ffffff" />
        ';
    }
    /**
     * Retrieve Northeastern chrome stylesheet
     * @return string
     */
    public static function chromeStyle()
    {
        return 'https://www.northeastern.edu/nuglobalutils/common/css/headerfooter.css';
    }

    /**
     * Retrieve Northeastern chrome script
     * @return string
     */
    public static function chromeScript()
    {
        return 'https://www.northeastern.edu/nuglobalutils/common/js/navigation-min.js';
    }

    /**
     * Retrieve Northeastern header
     * @return string
     */
    public static function chromeHeader()
    {
        return file_get_contents('https://www.northeastern.edu/resources/components/?return=main-menu&cache=no');
    }

    /**
     * Retrieve Northeastern footer
     * @return string
     */
    public static function chromeFooter($legacy = false, $class = '')
    {
        if ($legacy) {
            return '
                <footer class="section footer '. $class .'" role="contentinfo">
                    <a class="__logo" href="http://www.northeastern.edu" aria-label="Northeastern University logo">Northeastern University</a>
                    <div class="__body">
                      <ul class="__list">
                        <li><a target="_blank" rel="noreferrer" href="https://my.northeastern.edu/welcome">myNortheastern</a></li>
                        <li><a target="_blank" rel="noreferrer" href="https://prod-web.neu.edu/webapp6/employeelookup/public/main.action">Find Faculty &amp; Staff</a></li>
                        <li><a target="_blank" rel="noreferrer" href="http://www.northeastern.edu/neuhome/adminlinks/findaz.html">Find A-Z</a></li>
                        <li><a target="_blank" rel="noreferrer" href="http://www.northeastern.edu/emergency/index.html">Emergency Information</a></li>
                        <li><a target="_blank" rel="noreferrer" href="http://www.northeastern.edu/search/index.html">Search</a></li>
                        <li><a target="_blank" rel="noreferrer" href="http://www.northeastern.edu/privacy/index.html">Privacy</a></li>
                      </ul>
                      <div class="__copy">
                        360 Huntington Ave., Boston, Massachusetts 02115 &nbsp;&bull;&nbsp; 617.373.2000 &nbsp;&bull;&nbsp; TTY 617.373.3768<br>
                        &copy; '. date('Y') .' Northeastern University
                      </div>
                    </div>
                </footer>
            ';
        }

        return file_get_contents('https://www.northeastern.edu/resources/includes/?return=footer&cache=no');
    }

    /**
     * Retrieve Northeastern Google Tag Manager script
     * @return string
     */
    public static function gtmScript()
    {
        return "
            <!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-WGQLLJ');</script>
            <!-- End Google Tag Manager -->
        ";
    }

    /**
     * Retrieve Northeastern Google Tag Manager noscript
     * @return string
     */
    public static function gtmNoScript()
    {
        return '
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WGQLLJ"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
        ';
    }

    /**
     * Retrieve Google Analytics script
     * @param  $tracker Google Analytics tracker ID
     * @return string
     */
    public static function googleAnalytics($tracker)
    {
        return "
            <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
              ga('create', '{$tracker}', 'auto');
              ga('set', 'anonymizeIp', true);
              ga('send', 'pageview');
            </script>
        ";
    }
}
